<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
            ['nombre' => Str::random(10), 'precio' => Str::random(10),'descripcion' => Str::random(10)],
        ]);
    }
}